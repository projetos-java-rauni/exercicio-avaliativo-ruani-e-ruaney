package presenter;

//import model.Funcionario;
import com.mycompany.colletion.Connect;
import java.awt.event.ActionEvent;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Funcionario;
import model.FaltaAoTrabalho;
import view.ManterView;
import view.SalvoSucessoView;
//import view.SalvoSucessoView;

public class PresenterManter {

    private ManterView view;
    private Connect sql;

    public PresenterManter() {

        sql = new Connect();
        view = new ManterView();

        view.getBtnSalvar().addActionListener((ActionEvent e) -> {

            try {
                inserir();
                new SalvoSucessoView().setVisible(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(view,
                        "error: " + ex, "Aviso", JOptionPane.WARNING_MESSAGE);
            } catch (ParseException ex) {
                JOptionPane.showMessageDialog(view,
                        "error: " + ex, "Aviso", JOptionPane.WARNING_MESSAGE);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(view,
                        "error: " + ex, "Aviso", JOptionPane.WARNING_MESSAGE);
            }

        });

        view.getBtnFechar().addActionListener((ActionEvent e) -> {
            fechar();
        });

        setVisible();

    }

    public PresenterManter(int id) {
        sql = new Connect();
        view = new ManterView();

        Funcionario funcionario = null;
        try {
            funcionario = sql.selectById(id);
        } catch (SQLException ex) {
            Logger.getLogger(PresenterManter.class.getName()).log(Level.SEVERE, null, ex);
        }
        view.setTxtNome(funcionario.getNome());
        System.out.println(funcionario.getCargo());
        view.setTxtCargo(funcionario.getCargo());
        view.setTxtDataContrato(funcionario.getDataContrato().toString());
        view.setTxtDistancia(Double.toString(funcionario.getDistanciaTrabalho()));
        int faltaTot = 0;
        ArrayList<FaltaAoTrabalho> faltas = funcionario.getFaltas();
        for (FaltaAoTrabalho falta : faltas) {

            faltaTot += falta.getQuantidade();

        }
        view.setTxtFaltas(Integer.toString(faltaTot));
        view.setTxtIdade(Integer.toString(funcionario.getIdade()));
        view.setTxtSalario(Double.toString(funcionario.getSalarioBase()));
        view.setTxtTempoServico(Double.toString(funcionario.getTempoServico()));

        view.getBtnExcluir().addActionListener((ActionEvent e) -> {
            try {
                excluir(id);
            } catch (SQLException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        });

        view.getBtnEditar().addActionListener((

                ActionEvent e) -> {
            try {
                editar(id);
                new SalvoSucessoView().setVisible(true);
            } catch (SQLException | ParseException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        });
        view.getBtnFechar().addActionListener((
                ActionEvent e) -> {
            fechar();
        });

        setVisible();

    }

    private void inserir() throws SQLException, ParseException {

        String nome = view.getTxtNome().getText();
        int idade = parseInt(view.getTxtIdade().getText());
        Double salarioBase = parseDouble(view.getTxtSalario().getText());
        String cargo = view.getTxtCargo().getText();
        Double distanciaTrabalho = parseDouble(view.getTxtDistancia().getText());
        Double tempoServico = parseDouble(view.getTxtTempoServico().getText());
        String data = view.getTxtDataContrato().getText();

        sql.insert("employees", nome, idade, salarioBase, cargo, distanciaTrabalho, tempoServico, data);

    }

    private void editar(int id) throws SQLException, ParseException {

        String nome = view.getTxtNome().getText();
        int idade = parseInt(view.getTxtIdade().getText());
        Double salarioBase = parseDouble(view.getTxtSalario().getText());
        String cargo = view.getTxtCargo().getText();
        Double distanciaTrabalho = parseDouble(view.getTxtDistancia().getText());
        Double tempoServico = parseDouble(view.getTxtTempoServico().getText());
        String data = view.getTxtDataContrato().getText();
        sql.insert("employees", nome, idade, salarioBase, cargo, distanciaTrabalho, tempoServico, data);

    }

    private void excluir(int id) throws SQLException {
        sql.excluir(id);

    }

    private void fechar() {
        view.dispose();
    }

    private void setVisible() {
        if (view.isVisible()) {
            view.setVisible(false);
        } else {
            view.setVisible(true);
        }
    }
}
