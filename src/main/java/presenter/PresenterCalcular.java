package presenter;

import com.mycompany.colletion.Connect;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import model.Bonus;
import model.CalculadoraAssiduidade;
import model.CalculadoraTempoDeEmpresa;
import model.Funcionario;
import model.ICalculadoraBonus;
import view.CalcularView;

public class PresenterCalcular {

    private CalcularView view;
    private Connect sql;
    private DefaultTableModel tmFuncionarios;
    private ArrayList<ICalculadoraBonus> calc = new ArrayList();

    public PresenterCalcular() {
        view = new CalcularView();
        sql = new Connect();
        view.getBtnFechar().addActionListener((ActionEvent e) -> {
            fechar();
        });
        view.getBtnListarTodos().addActionListener((ActionEvent e) -> {
            try {
                buscar();
            } catch (SQLException ex) {
                Logger.getLogger(PresenterCalcular.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        view.setVisible(true);
    }

    private void fechar() {
        view.dispose();
    }

    private void buscar() throws SQLException {
        ArrayList<Funcionario> funcionarios = new ArrayList();
        funcionarios = sql.getAllFromTableFuncionarios();
        setTable(funcionarios);
    }

    private void setTable(ArrayList<Funcionario> funcionarios) {
        tmFuncionarios = new DefaultTableModel(
                new Object[][]{},
                new String[]{"Funcionario", "Data", "Salario base (R$)", "Bonus tot(R$)", "Salario tot(R$)"}
        );
        view.getTblFuncionarios().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tmFuncionarios.setRowCount(0);
        calculaBonus(funcionarios);
        ListIterator<Funcionario> it = funcionarios.listIterator();
        while (it.hasNext()) {
            Funcionario funcionario = it.next();
            tmFuncionarios.addRow(new Object[]{funcionario.getNome(), funcionario.getDataContrato(), funcionario.getSalarioBase(), funcionario.getBonusTotal(), funcionario.getSalarioTotal()});
        }
        view.getTblFuncionarios().setModel(tmFuncionarios);
    }

    private void calculaBonus(ArrayList<Funcionario> funcionarios) {
        calc.add(new CalculadoraAssiduidade());
        calc.add(new CalculadoraTempoDeEmpresa());

        for (Funcionario funcionario : funcionarios) {
            for (ICalculadoraBonus calcular : calc) {
                calcular.calcula(funcionario);
            }
            funcionario.calculaBonus();
            funcionario.calculaSalario();

        }

    }
}
