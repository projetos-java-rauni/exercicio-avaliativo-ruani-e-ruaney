/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package presenter;

import com.mycompany.colletion.Connect;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import model.Funcionario;
import view.BuscarView;
import view.ManterView;

/**
 *
 * @author ruanr
 */
public class PresenterBuscar {

    private BuscarView view;
    private Connect sql;
    private DefaultTableModel tmFuncionarios;

    public PresenterBuscar() {
        try {
            sql = new Connect();
            view = new BuscarView();
            ArrayList<Funcionario> funcionarios = sql.getAllFromTableFuncionarios();
            setTable(funcionarios);
            view.getBtnBuscar().addActionListener((ActionEvent e) -> {
                try {
                    Buscar();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(view,
                            "erro no sql: " + e, "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            });
            view.getBtnFechar().addActionListener((ActionEvent e) -> {
                Fechar();
            });
            view.getBtnNovo().addActionListener((ActionEvent e) -> {
                new ManterView().setVisible(true);
//                ManterView presenter = new ManterView();
            });
            view.getBtnVisualizar().addActionListener((ActionEvent e) -> {
                int id = Integer.parseInt(view.getTblFuncionarios().getValueAt(view.getTblFuncionarios().getSelectedRow(), 0).toString());
                new PresenterManter(id);
//              ManterView presenter = new ManterView();
            });
            view.setVisible(true);
        } catch (Exception e) {
            JOptionPane optionPane = new JOptionPane(e);
            javax.swing.JDialog dialog = optionPane.createDialog(view, "Aviso");
            dialog.setAlwaysOnTop(true);
            dialog.setVisible(true);
        }
    }

    private void Buscar() throws SQLException {
        ArrayList<Funcionario> funcionarios = new ArrayList();
        String nome = view.getTxtNome().getText();
        if (nome.isEmpty()) {
            JOptionPane.showMessageDialog(view,
                    "informe um nome", "Aviso", JOptionPane.WARNING_MESSAGE);
            return;
        }
        funcionarios = sql.selectByNome(nome);
        setTable(funcionarios);
    }

    private void Fechar() {
        view.dispose();
    }

    private void setTable(ArrayList<Funcionario> funcionarios) {
        tmFuncionarios = new DefaultTableModel(
                new Object[][]{},
                new String[]{"ID", "Nome", "Idade", "Cargo", "Salario Base (R$)"}
        );
        view.getTblFuncionarios().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tmFuncionarios.setRowCount(0);
        ListIterator<Funcionario> it = funcionarios.listIterator();

        while (it.hasNext()) {
            Funcionario funcionario = it.next();
            tmFuncionarios.addRow(new Object[]{funcionario.getId(), funcionario.getNome(), funcionario.getIdade(), funcionario.getCargo(), funcionario.getSalarioBase()});
        }
        view.getTblFuncionarios().setModel(tmFuncionarios);
    }
}
