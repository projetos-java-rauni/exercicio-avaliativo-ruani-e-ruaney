package model;

import java.time.LocalDate;
import java.util.ArrayList;




public class CalculadoraAssiduidade implements ICalculadoraBonus {
 
    @Override
    public void calcula(Funcionario F){
        
        ArrayList<FaltaAoTrabalho> faltas = F.getFaltas();
        String typeBonus = "Assiduidade";
        Double salario = F.getSalarioBase();
        
        for(FaltaAoTrabalho falta : faltas){
            if(falta.getQuantidade() == 0){
                F.add(new Bonus(typeBonus,  salario*1.05 - salario, LocalDate.now()));
            }
            else if (falta.getQuantidade() >= 1 || falta.getQuantidade() <= 3){
           
                F.add(new Bonus(typeBonus, salario*1.03 - salario, LocalDate.now()));
            }
            else if (falta.getQuantidade() >= 4 || falta.getQuantidade() <= 5){
                F.add(new Bonus(typeBonus,  salario*1.01 - salario, LocalDate.now()));
            }            
            else if (falta.getQuantidade() >= 6){
                F.add(new Bonus(typeBonus,  0.0, LocalDate.now()));
            }            
        }
        }
       
    }

