/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.time.LocalDate;

/**
 *
 * @author ruanr
 */
public class FaltaAoTrabalho {

    /**
     * @return the getQuantidade
     */
    public int getQuantidade() {
        return this.quantidade;
    }
    
    private int quantidade;
    private LocalDate mes;

    public FaltaAoTrabalho(int quantidade, LocalDate mes) {
        this.quantidade = quantidade;
        this.mes = mes;
    }

    public void AlteraQuantidade(int quantidade, LocalDate mes) {
        this.quantidade = quantidade;
        this.mes = mes;
    }

    public LocalDate getMes(LocalDate mes) {
        return this.mes;
    }

}
