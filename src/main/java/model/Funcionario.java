/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import static java.lang.Double.parseDouble;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author ruanr
 */
public class Funcionario {

    /**
     * @return the distanciaTrabalho
     */
    public Double getDistanciaTrabalho() {
        return distanciaTrabalho;
    }

    /**
     * @return the bonusTotal
     */
    public Double getBonusTotal() {
        return bonusTotal;
    }

    /**
     * @return the salarioTotal
     */
    public Double getSalarioTotal() {
        return salarioTotal;
    }

    /**
     * @return the tempoServico
     */
    public Double getTempoServico() {
        return tempoServico;
    }

    /**
     * @return the dataContrato
     */
    public LocalDate getDataContrato() {
        return dataContrato;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void     setId(int id) {
        this.id = id;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @return the salarioBase
     */
    public Double getSalarioBase() {
        return salarioBase;
    }

    /**
     * @return the getFaltas
     */
    public ArrayList<FaltaAoTrabalho> getFaltas() {
        return faltas;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the cargo
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * @return the bonus
     */
    public ArrayList<Bonus> getBonus() {
        return bonus;
    }

    private int id;
    private String nome;
    private int idade;
    private Double salarioBase;
    private String cargo;
    private Double distanciaTrabalho;
    private Double tempoServico;
    private LocalDate dataContrato;
    private ArrayList<Bonus> bonus;
    private ArrayList<FaltaAoTrabalho> faltas;
    private Double salarioTotal;
    private Double bonusTotal;

    public Funcionario(String nome, int idade, Double salarioBase, String cargo, Double distanciaTrabalho, Double tempoServico, LocalDate dataContrato) {
        this.nome = nome;
        this.idade = idade;
        this.salarioBase = salarioBase;
        this.cargo = cargo;
        this.distanciaTrabalho = distanciaTrabalho;
        this.tempoServico = tempoServico;
        this.dataContrato = dataContrato;
        this.bonus = new ArrayList();
        this.faltas = new ArrayList();
        this.salarioTotal = 0.0;
    }

    public void add(Bonus bonus) {
        //adicionar obnus.push
        this.bonus.add(bonus);
    }

    public void Remove(Bonus bonus) {

    }

    public void add(FaltaAoTrabalho falta) {
        this.faltas.add(falta);
    }

    public void Remove(FaltaAoTrabalho falta) {

    }

    public void calculaSalario() {
        //salario total = salarioBase + Bonus recebidos
        Double bonusTotal = 0.0;
        for (Bonus b : this.bonus) {
            bonusTotal += b.getValor();
        }
        this.salarioTotal = this.salarioBase + bonusTotal;
    }

    public void calculaBonus() {
        //salario total = salarioBase + Bonus recebidos
        Double bonusTotal = 0.0;
        for (Bonus b : this.bonus) {
            bonusTotal += b.getValor();
        }
        this.bonusTotal = bonusTotal;
    }

}
