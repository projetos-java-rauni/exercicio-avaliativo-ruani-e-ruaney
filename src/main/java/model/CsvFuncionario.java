/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ruanr
 */
public class CsvFuncionario {
    private int id;
    private String nome;
    private String cargo;
    private Double salarioBase;
    private Double distanciaTrabalho;
    private Double tempoServico;
    private String dataContrato;
}
