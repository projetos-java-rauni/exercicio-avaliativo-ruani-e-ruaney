package model;

import java.time.LocalDate;

public class CalculadoraTempoDeEmpresa implements ICalculadoraBonus {

    @Override
    public void calcula(Funcionario F) {
        Double tempoEmpresa = F.getTempoServico();
        Double salarioBase = F.getSalarioBase();
        String tipoBonus = "Tempo de Empresa";
        if (tempoEmpresa == 0) {
            F.add(new Bonus(tipoBonus, 0.0, LocalDate.now()));
        } else if (tempoEmpresa >= 1 || tempoEmpresa <= 5) {
            F.add(new Bonus(tipoBonus, salarioBase * 1.02 - salarioBase, LocalDate.now()));
        } else if (tempoEmpresa >= 6 || tempoEmpresa <= 10) {
            F.add(new Bonus(tipoBonus, salarioBase * 1.03 - salarioBase, LocalDate.now()));
        } else if (tempoEmpresa >= 11 || tempoEmpresa <= 15) {
            F.add(new Bonus(tipoBonus, salarioBase * 1.08 - salarioBase, LocalDate.now()));
        } else if (tempoEmpresa >= 16 || tempoEmpresa <= 20) {
            F.add(new Bonus(tipoBonus, salarioBase * 1.1 - salarioBase, LocalDate.now()));
        } else if (tempoEmpresa > 20) {
            F.add(new Bonus(tipoBonus, salarioBase * 1.1 - salarioBase, LocalDate.now()));
        }
    }
}
