package com.mycompany.colletion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ruanr
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import model.FaltaAoTrabalho;
import model.Funcionario;

/**
 *
 * @author rauni
 */
public class Connect {

    private static Connection conn = null;

    public Connect() {
        try {
            String url = "jdbc:sqlite:Banco.db";
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println("message: " + e);
        }
    }

    public Connection getConnection() {
        return conn;
    }
    public void excluir(int id) throws SQLException{
        String sql = "SELECT employees,rowid DELETE FROM employees WHERE rowid="+id;
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.executeUpdate();

    }
    public void insert(String table, String nome, int idade, double salarioBase, String cargo, double distanciaTrabalho,
            Double tempoServico, String dataContrato) throws SQLException, ParseException {
        String sql = "INSERT INTO " + table
                + "(nome,idade, salarioBase,cargo,distanciaTrabalho,tempoServico,dataContrato) VALUES(?,?,?,?,?,?,?)";
        // System.out.println(java.sql.Date.valueOf(dataContrato.format(DateTimeFormatter.ofPattern("AAAA-MM-DD"))));
        // java.sql.Date sqlDate = java.sql.Date.valueOf(dataContrato);
        DateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
        java.sql.Date sqlDate = null;
        sqlDate = new java.sql.Date(fmt.parse(dataContrato).getTime());
        if (sqlDate == null) {
            throw new ParseException("informe data do tipo dd-MM-yyyy", 0);
        }
        PreparedStatement pstmt = conn.prepareStatement(sql);

        pstmt.setString(1, nome);
        pstmt.setInt(2, idade);
        pstmt.setDouble(3, salarioBase);
        pstmt.setString(4, cargo);
        pstmt.setDouble(5, distanciaTrabalho);
        pstmt.setDouble(6, tempoServico);
        pstmt.setDate(7, sqlDate);
        pstmt.executeUpdate();

    }

    public void updateEmployes(int id, String nome, int idade, double salarioBase, String cargo, double distanciaTrabalho,
            Double tempoServico, String dataContrato) throws SQLException, ParseException {
        String sql = "UPDATE employees SET "
                + "(nome,idade, salarioBase,cargo,distanciaTrabalho,tempoServico,dataContrato) VALUES(?,?,?,?,?,?,?) "
                + "WHERE rowid = ?";
        // System.out.println(java.sql.Date.valueOf(dataContrato.format(DateTimeFormatter.ofPattern("AAAA-MM-DD"))));
        // java.sql.Date sqlDate = java.sql.Date.valueOf(dataContrato);
        DateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
        java.sql.Date sqlDate = null;
        sqlDate = new java.sql.Date(fmt.parse(dataContrato).getTime());
        if (sqlDate == null) {
            throw new ParseException("informe data do tipo dd-MM-yyyy", 0);
        }
        PreparedStatement pstmt = conn.prepareStatement(sql);

        pstmt.setString(1, nome);
        pstmt.setInt(2, idade);
        pstmt.setDouble(3, salarioBase);
        pstmt.setString(4, cargo);
        pstmt.setDouble(5, distanciaTrabalho);
        pstmt.setDouble(6, tempoServico);
        pstmt.setDate(7, sqlDate);
        pstmt.setInt(8, id);

        pstmt.executeUpdate();

    }

    public ArrayList<Funcionario> getAllFromTableFuncionarios() throws SQLException {
        String sql = "SELECT rowid, * FROM employees";

        // Connection conn = this.connect();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        ArrayList<Funcionario> funcionarios = new ArrayList();
        // loop through the result set
        while (rs.next()) {
            Funcionario tempFuncionario = new Funcionario(
                    rs.getString("nome"), rs.getInt("idade"), rs.getDouble("salarioBase"),
                    rs.getString("cargo"), rs.getDouble("distanciaTrabalho"),
                    rs.getDouble("tempoServico"), rs.getDate("dataContrato").toLocalDate());
            tempFuncionario.setId(rs.getInt("rowid"));
            createFaltas(tempFuncionario, 0);
            funcionarios.add(tempFuncionario);
        }
        return funcionarios;
    }

    public void toStringAllEmployees() throws SQLException {
        String sql = "SELECT rowid, * FROM employees";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            System.out.println(
                    "ID: " + rs.getInt("rowid") + "\t"
                            + "NOME: " + rs.getString("nome") + "\t"
                            + "IDADE: " + rs.getString("idade") + "\t"
                            + "SALARIOBASE: " + rs.getDouble("salarioBase") + "\t"
                            + "CARGO: " + rs.getString("cargo") + "\t"
                            + "DISTANCIA TRABALHO: " + rs.getDouble("distanciaTrabalho") + "\t"
                            + "TEMPO SERVICO: " + rs.getDouble("tempoServico") + "\t"
                            + "DATA CONTRATO: " + rs.getDate("dataContrato") + "\t");
        }
    }

    public ArrayList<Funcionario> selectByNome(String nome) throws SQLException {
        String sql = "SELECT rowid, * FROM employees WHERE nome LIKE " + "'%" + nome + "%'";

        Statement stmt = conn.createStatement();

        ResultSet rs = stmt.executeQuery(sql);
        ArrayList<Funcionario> funcionarios = new ArrayList();
        while (rs.next()) {
            Funcionario tempFuncionario = new Funcionario(
                    rs.getString("nome"), rs.getInt("idade"), rs.getDouble("salarioBase"),
                    rs.getString("cargo"), rs.getDouble("distanciaTrabalho"),
                    rs.getDouble("tempoServico"), rs.getDate("dataContrato").toLocalDate());
            tempFuncionario.setId(rs.getInt("rowid"));
            createFaltas(tempFuncionario, 0);
            funcionarios.add(tempFuncionario);
        }
        return funcionarios;
    }

    public Funcionario selectById(int id) throws SQLException {
        String sql = "SELECT rowid, * FROM employees WHERE rowid=" + id;

        PreparedStatement pstmt = conn.prepareStatement(sql);
        Statement stmt = conn.createStatement();

        ResultSet rs = stmt.executeQuery(sql);

        Funcionario tempFuncionario = new Funcionario(
                rs.getString("nome"), rs.getInt("idade"), rs.getDouble("salarioBase"),
                rs.getString("cargo"), rs.getDouble("distanciaTrabalho"),
                rs.getDouble("tempoServico"), rs.getDate("dataContrato").toLocalDate());
        tempFuncionario.setId(rs.getInt("rowid"));
        createFaltas(tempFuncionario, 0);
        return tempFuncionario;
    }

    public void deletarFuncionario(int id) throws SQLException {
        String sql = "DELETE FROM employees WHERE id=" + id;
        PreparedStatement pstmt = conn.prepareStatement(sql);
        Statement stmt = conn.createStatement();
        System.out.println(stmt);
    }

    public void DropTable(String table) throws SQLException {
        Statement stmt = conn.createStatement();
        String sqlCommand = "DROP TABLE IF EXISTS " + table;
        System.out.println("output : " + stmt.executeUpdate(sqlCommand));
    }

    public void createTableFuncionarios() throws SQLException {
        Statement statement = conn.createStatement();
        statement.executeUpdate("CREATE TABLE employees (nome STRING, idade INT,"
                + "salariobase REAL, cargo STRING, distanciaTrabalho REAL,"
                + " tempoServico REAL, dataContrato DATE)");
    }

    private void createFaltas(Funcionario funcionario, int faltaQuantidade) {
        Random random = new Random();
        int numberMes = random.nextInt(12) + 1;
        if (faltaQuantidade <= 0) {
            faltaQuantidade = random.nextInt(30) + 1;
        }
        funcionario.add(new FaltaAoTrabalho(faltaQuantidade, LocalDate.of(1, numberMes, 1)));

    }
}
