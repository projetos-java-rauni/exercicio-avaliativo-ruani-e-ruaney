/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.colletion;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import model.CsvFuncionario;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import static java.lang.Double.parseDouble;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author ruanr
 */
public class Csv {

    String csvFile = "funcionarios.csv";
    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ",";
    Connect sql;

    public Csv() throws SQLException, ParseException {
        try {
            sql = new Connect();
            FileReader filereader = new FileReader(csvFile);
            CSVReader csvReader
                    = new CSVReaderBuilder(filereader).withSkipLines(1).build();
            List<String[]> allData = csvReader.readAll();

            // le linha por linha
            for (String[] row : allData) {
                System.out.println(row[0]);
                sql.insert("employees", row[1], 0, parseDouble(row[3]),
                        row[2], parseDouble(row[4]),
                        parseDouble(row[5]), row[6]);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
